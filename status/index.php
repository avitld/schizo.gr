<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Service Status - schizo.gr</title>
        <link rel="icon" type="img/png" href="/static/img/logo.png">
        <meta name="description" content="Schizoid website about free software.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/static/css/style.css">
    </head>
    <body>
            <header>
                <a href="/">
                    <img id="hlogo" src="/static/img/logo.png">
                </a>
                <nav>
                    <a href="/status/">Status</a>
                    <a href="/contact/">Contact</a>
                    <a href="/articles/">Articles</a>
                    <a href="/guides/">Guides</a>
                    <a href="/news/">News</a>
                </nav>
            </header>
            <h1>Instance list and status</h1>
            <p>
                Here you will find a list of services hosted on this server.
            </p>
            <div class="instance-list">
            <?php
                    $instanceList = file_get_contents(__DIR__ . '/instances.json');
                    $data = json_decode($instanceList, true);

                    if ($data === null) {
                        echo "Error getting instance list.";
                        $data = [
                            "searchtld" => "https://searchtld.com",
                            "matrix" => "https://mt.schizo.gr",
                            "forgejo" => "https://git.schizo.gr"
                        ];
                    }

                    foreach ($data as $instanceName => $instanceUrl) {
                        echo "<div class=\"instance-l\">";
                        echo "<a href=\"$instanceUrl\">$instanceName</a>";
						echo "<label>Status:</label>";
                        checkUptime($instanceUrl);
                        echo "</div>";
                    }

                    function checkUptime($url) {
                        $response = file_get_contents($url);
                
                        $httpCode = $http_response_header[0];
                        if (strpos($httpCode, '200') !== false) {
                            echo "<p class=\"activitystats\" id=\"activity-status-active\">Active!</p>";
                        } else {
                            echo "<p class=\"activitystats\" id=\"activity-status-inactive\">Down.</p>";
                        }
                    }
                    ?>
            </div>
            <p>
                If you want to request a specific service to be added, just e-mail me 
                or contact me on Matrix.
            </p>
            <p>
                If an instance is down but shown as active please contact me <strong>through matrix only
                </strong>
            </p>
    </body>
</html>
