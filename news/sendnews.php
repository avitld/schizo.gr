<?php
    function getUserInput($prompt) {
        echo $prompt . ': ';
        return trim(fgets(STDIN));
    }

    $jsonFilePath = 'news.json';

    $log = array();

    $title = getUserInput('Announcement Title');
    $text = getUserInput('Announcement Text');
    $date = date('Y-m-d H:i:s');

    $entry = array(
        'title' => $title,
        'prompt' => $text,
        'date' => $date,
    );

    array_unshift($log, $entry);

    $jsonData = json_encode($log, JSON_PRETTY_PRINT);
    file_put_contents($jsonFilePath, $jsonData);

    echo "Announcement sent!\n";
?>
