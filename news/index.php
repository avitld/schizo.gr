<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>News - schizo.gr</title>
        <link rel="icon" type="img/png" href="/static/img/logo.png">
        <meta name="description" content="Schizoid website about free software.">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/static/css/style.css">
    </head>
    <body style="height: 100%;">
            <header>
                <a href="/">
                    <img id="hlogo" src="/static/img/logo.png">
                </a>
                <nav>
                    <a href="/status/">Status</a>
                    <a href="/contact/">Contact</a>
                    <a href="/articles/">Articles</a>
                    <a href="/guides/">Guides</a>
                    <a href="/news/">News</a>
                </nav>
            </header>
            <h1>News</h1>
            <p>
                Here you'll find announcements and changelogs regarding
                the website and the services hosted here. Announcements
                are posted using a PHP script on our server.
            </p>
            <?php
                function getJsonData($filePath) {
                    $jsonData = file_get_contents($filePath);
                    return json_decode($jsonData, true);
                }

                $jsonFilePath = 'news.json';

                $prompts = getJsonData($jsonFilePath);

                foreach ($prompts as $entry) {
                    $title = htmlspecialchars($entry['title']);
                    $prompt = htmlspecialchars($entry['prompt']);
                    $date = htmlspecialchars($entry['date']);

                    echo '<div class="news-prompt">';
                    echo '<label id="date" for="title">' . $date . '</label>';
                    echo '<p id="title">' . $title . '</p>';
                    echo '<p id="desc">' . $prompt . '</p>';
                    echo '</div>';
                }
            ?>
    </body>
</html>